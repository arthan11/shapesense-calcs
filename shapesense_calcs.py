from datetime import datetime, date
from dateutil.relativedelta import relativedelta

# Genders
FEMALE = 0
MALE = 1

# Activity Levels
SEDENTARY = 1.2 # Very little or no daily physical activity.
LIGHTLY_ACTIVE = 1.375 # Light physical activity 1 to 3 days per week.
MODERATELY_ACTIVE = 1.55 # Moderate physical activity 3 to 5 days per week.
VERY_ACTIVE = 1.725 # Hard physical activity 6 to 7 days per week.
EXTREMELY_ACTIVE = 1.9 # Hard physical activity every day of the week.

VO2maxWomenValues = [
        [20, 35, 39, 43, 49, 50],
        [30, 33, 36, 40, 45, 46],
        [40, 31, 34, 38, 44, 45],
        [50, 24, 28, 30, 34, 35],
        [60, 25, 28, 31, 35, 36],
        [70, 23, 26, 29, 35, 36]
    ]

VO2maxMenValues = [
        [20, 41, 45, 50, 55, 56],
        [30, 40, 43, 47, 53, 54],
        [40, 37, 41, 45, 52, 53],
        [50, 34, 37, 42, 49, 50],
        [60, 30, 34, 38, 45, 46],
        [70, 27, 30, 35, 41, 42]
    ]

VO2maxClassifications = [
    'Poor',
    'Fair',
    'Good',
    'Excellent',
    'Superior',
    ]

class ShapesenseCalc(object):
    def __init__(self):
        pass

    def VO2maxClassification(self, Age, Gender, VO2max):
        VO2max = int(VO2max)
        if Age < 20:
            print 'Age lower than 20'
            return None
        elif Age > 79:
            print 'Age higher than 79'
            return None
        if Gender == MALE:
            values = VO2maxMenValues
        else:
            values = VO2maxWomenValues
        row = []
        for v in values:
            if (Age >= v[0]) and (Age < v[0]+10):
                row = v[1:]
                break
        for i, v in enumerate(row):
            if (VO2max <= v):
                return VO2maxClassifications[i]
        return VO2maxClassifications[-1]




    def HR2Kcal(self, Gender, Age, Weight, ActivityDuration, HR, VO2max=None):
        '''
        Heart Rate Based Calorie Burn Calculator.
        This calculator provides gross calorie burn estimates.
        HR = Heart rate (in beats/minute)
        VO2max = Maximal oxygen consumption
        Weight = Weight (in kilograms)
        Age = Age (in years)
        ActivityDuration = Exercise duration time (in hours)
        http://www.shapesense.com/fitness-exercise/calculators/heart-rate-based-calorie-burn-calculator.aspx
        '''

        if (HR < 121) or (HR > 200):
            print 'Heart rate must be between 121 and 200 bpm'
            return None

        Age = float(Age)
        Weight = float(Weight)
        ActivityDuration = float(ActivityDuration)
        HR = float(HR)
        if VO2max != None:
            VO2max = float(VO2max)
        if not VO2max:
            if Gender == MALE:
                ret = ((-55.0969 + (0.6309 * HR) + (0.1988 * Weight) + (0.2017 * Age))/4.184) * 60 * ActivityDuration
            else:
                ret = ((-20.4022 + (0.4472 * HR) - (0.1263 * Weight) + (0.074 * Age))/4.184) * 60 * ActivityDuration
        else:
            if Gender == MALE:
                ret = ((-95.7735 + (0.634 * HR) + (0.404 * VO2max) + (0.394 * Weight) + (0.271 * Age))/4.184) * 60 * ActivityDuration
            else:
                ret = ((-59.3954 + (0.45 * HR) + (0.380 * VO2max) + (0.103 * Weight) + (0.274 * Age))/4.184) * 60 * ActivityDuration
        return int(round(ret))

    def MaxHR(self, Age):
        '''
        Maximum Heart Rate Based on Age
        http://www.shapesense.com/fitness-exercise/calculators/heart-rate-based-calorie-burn-calculator.aspx
        '''
        return 208 - (0.7 * float(Age))

    def DailyKcal(self, Gender, Age, Weight, Height, ActivityLevel):
        '''
        Daily Caloric Expenditure
        Height = Height in Centimetres
        Weight = Weight in Kilograms
        http://www.shapesense.com/fitness-exercise/calculators/daily-caloric-expenditure-calculator.aspx
        '''
        Age = float(Age)
        Weight = float(Weight)
        Height = float(Height)
        if Gender == FEMALE:
            dce = ActivityLevel * ((9.56 * Weight) + (1.85 * Height) - 4.68 * Age + 655)
        else:
            dce = ActivityLevel * ((13.75 * Weight) + (5 * Height) - 6.76 * Age + 66)
        return int(round(dce))

    def BMR(self, Gender, Age, Weight, Height):
        '''
        Basal Metabolic Rate
        Height = Height in Centimetres
        Weight = Weight in Kilograms
        http://www.shapesense.com/fitness-exercise/calculators/bmr-calculator.aspx
        '''
        Age = float(Age)
        Weight = float(Weight)
        Height = float(Height)
        if Gender == FEMALE:
            bmr = (9.56 * Weight) + (1.85 * Height) - (4.68 * Age) + 655
        else:
            bmr = (13.75 * Weight) + (5 * Height) - (6.76 * Age) + 66
        return int(round(bmr))

    def RMR(self, Gender, Age, Weight, Height):
        '''
        Resting Metabolic Rate Calculator
        Height = Height in Centimetres
        Weight = Weight in Kilograms
        http://www.shapesense.com/fitness-exercise/calculators/resting-metabolic-rate-calculator.aspx
        '''
        Age = float(Age)
        Weight = float(Weight)
        Height = float(Height)
        if Gender == MALE:
            rmr = ((13.75 * Weight) + (5 * Height) - (6.76 * Age) + 66) * 1.1
        else:
            rmr = ((9.56 * Weight) + (1.85 * Height) - (4.68 * Age) + 655) * 1.1
        return int(round(rmr))

    def BMI(self, Weight, Height):
        '''
        Body Mass Index Calculator
        Height = Height in Metres
        Weight = Weight in Kilograms
        http://www.shapesense.com/fitness-exercise/calculators/body-mass-index-calculator.aspx
        '''
        CLASSIFICATIONS = [[0, 18.5, 'underweight'],
                           [18.5, 25, 'normal'],
                           [25, 30, 'overweight'],
                           [30, 99, 'obese']
                          ]
        Weight = float(Weight)
        Height = float(Height)/100 # cm > m
        bmi = Weight / (Height * Height)
        txt = ''
        for clas in CLASSIFICATIONS:
            if (bmi >= clas[0]) and (bmi < clas[1]):
                txt = clas[2]
                break
        return round(bmi, 2), txt

    def HRReserve(self, Age, RestingHR20s):
        '''
        Heart Rate Reserve
        Heart rate reserve is the difference between your maximum heart rate and your resting heart rate.
        HRR = Heart Rate Reserve (beats/minute)
        MHR = Maximum Heart Rate (beats/minute) = 208 - (0.7 x Age)
        RHR = 20 Second Resting Heart Rate (beats/minute)
        http://www.shapesense.com/fitness-exercise/calculators/heart-rate-reserve-calculator.aspx
        '''
        MHR = self.MaxHR(Age)
        RHR = float(RestingHR20s * 3.0)
        HRR = MHR - RHR
        #print MHR, RHR
        return int(round(HRR))

    def TargetHR(self, Age):
        '''
        Target Heart Rate
        This target heart rate calculator estimates your personal age-predicted exercise target heart rate training zones.
        http://www.shapesense.com/fitness-exercise/calculators/target-heart-rate-calculator.aspx
        '''
        MHR = round(self.MaxHR(Age))
        ACSMExerciseIntensityLevels = [
                [35, 55, 'light'],
                [55,70,'moderate'],
                [70, 90, 'heavy'],
                [90, 100, 'very heavy'],
                [100, 101, 'maximal']
            ]

        lvls = {}
        for level in ACSMExerciseIntensityLevels:
            min =  int(round(MHR * float(level[0]) / 100.0))
            max =  int(round(MHR * float(level[1]-1) / 100.0))
            lvls[level[2]] = {'min': min, 'max':max}

        def PrintLevels(lvls):
            ret = ''
            for i, level in enumerate(ACSMExerciseIntensityLevels):
                name = level[2]
                if i < 3:
                    ret += '{}: From {} to {} beats/minute.\n'.format(name, lvls[name]['min'], lvls[name]['max'])
                elif i == 3:
                    ret += '{}: Above {} beats/minute.\n'.format(name, lvls[name]['min'])
                else:
                    ret += '{}: {} beats/minute.\n'.format(name, lvls[name]['min'])
            return ret
        return PrintLevels(lvls)

    def RHR2VO2max(self, Age, RestingHR20s):
        '''
        VO2max Calculator - Resting Heart Rate Based
        MHR = Maximum heart rate
        RHR = Resting heart rate (beats/minute) = 20 second heart rate x 3
        http://www.shapesense.com/fitness-exercise/calculators/vo2max-calculator.aspx
        '''
        MHR = self.MaxHR(Age)
        RHR = float(RestingHR20s * 3)
        VO2max = 15.3 * (MHR/RHR)
        return int(round(VO2max))

    def MHRProc2VO2maxProc(self, MHRProc):
        return round(1.5472 * float(MHRProc) - 57.53)

    def FatVsCarbohydrate(self, Gender, Age, HeartRate):
        '''
        Fat Versus Carbohydrate Utilization During Exercise Calculator
        Allows you to estimate, based on your exercise intensity level as measured by your
        heart rate, the relative contributions of fat and carbohydrates to your total energy expenditure during exercise.
        F = Fat utilization as a percentage of total energy expenditure
        CHO = Carbohydrate utilization as a percentage of total energy expenditure
        %VO2max = Percentage of maximal oxygen consumption
        http://www.shapesense.com/fitness-exercise/calculators//fat-versus-carbohydrate-utilization-during-exercise-calculator.aspx
        '''
        MHR = round(self.MaxHR(Age))
        MHRProc = round(float(HeartRate) / MHR * 100)
        VO2maxProc = self.MHRProc2VO2maxProc(MHRProc)
        print MHR, MHRProc, VO2maxProc
        if VO2maxProc < 41:
            print 'VO2maxProc lower than 41%'
            return None
        elif VO2maxProc > 97:
            print 'VO2maxProc higher than 97%'
            return None

        F = 0.0
        CHO = 0.0
        if Gender == MALE:
            if VO2maxProc < 48:
                F = -0.0497 * VO2maxProc * VO2maxProc + 3.8528 * VO2maxProc - 23.55
                CHO = 0.0497 * VO2maxProc * VO2maxProc - 3.8528 * VO2maxProc + 123.55
            else:
                F = -1.2746 * VO2maxProc + 108.24
                CHO = 1.2746 * VO2maxProc - 8.24
        else:
            if VO2maxProc < 48:
                F = -0.0497 * VO2maxProc * VO2maxProc + 3.8528 * VO2maxProc - 13.55
                CHO = 0.0497 * VO2maxProc * VO2maxProc - 3.8528 * VO2maxProc + 113.55
            else:
                F = -1.59 * VO2maxProc + 135.11
                CHO = 1.59 * VO2maxProc - 35.11

        if F < 0:
            F = 0.0
            CHO = 100.0
        return int(round(F)), int(round(CHO))

    def OneRepetitionMaximum(self, WeightLifted, RepetitionsCompleted):
        '''
        1RM Calculator
        This 1RM calculator is considered to be reasonably accurate for entries of up to 10 repetitions.
        The calculator will not determine your 1RM for entered repetition values of greater than 10.
        http://www.shapesense.com/fitness-exercise/calculators/1rm-calculator.aspx
        '''
        W = float(WeightLifted)
        R = float(RepetitionsCompleted)
        if R > 10:
            print 'max 10 repetitions'
            return None
        Brzycki = round(W * (36 / (37 - R)))
        Epley = round(W * (1 + 0.0333 * R))
        Lander = round((100 * W) / (101.3 - 2.67123 * R))
        return int(round((Brzycki + Epley + Lander) / 3))

    def Age(self, birth_date):
        return relativedelta(datetime.now(), birth_date).years


if __name__ == '__main__':
    calc = ShapesenseCalc()

    #ActivityDuration = 40
    #ActivityDuration = float(ActivityDuration) / 60.0
    #x = calc.HR2Kcal(MALE, 28, 80, ActivityDuration, 151)

    #x = calc.MaxHR(28)
    #x = calc.DailyKcal(FEMALE, 28, 88, 175, SEDENTARY)
    #x = calc.BMR(FEMALE, 20, 60, 175)
    #x = calc.RMR(FEMALE, 20, 60, 175)
    #x = calc.BMI(88, 1.75)
    #x = calc.HRReserve(28, 20)
    #x = calc.TargetHR(28)

    #x = calc.RHR2VO2max(28, RHR/3)
    # print calc.VO2maxClassification(22, FEMALE, x)
    #x = calc.FatVsCarbohydrate(MALE, 28, 160)
    #x = calc.OneRepetitionMaximum(230, 9)

    RHR = 70
    birth_date = date(1980, 01, 01)
    age = calc.Age(birth_date)
    gender = MALE
    weight = 70.0
    height = 180

    VO2Max = calc.RHR2VO2max(age, RHR/3)
    VO2MaxClassification = calc.VO2maxClassification(age, gender, VO2Max)
    BMR = calc.BMR(gender, age, weight, height)
    daily_kcal = calc.DailyKcal(gender, age, weight, height, SEDENTARY)
    max_hr = calc.MaxHR(age)
    RMR = calc.RMR(gender, age, weight, height)
    BMI = calc.BMI(weight, height)
    hr_reserve = calc.HRReserve(age, RHR/3)
    target_hr = calc.TargetHR(age)

    print 'VO2Max: {} [{}]'.format(VO2Max, VO2MaxClassification)
    print 'HR Reserve: {}'.format(hr_reserve)
    print 'Max HR: {}'.format(max_hr)
    print target_hr


    print 'Body Mass Index: {} [{}]'.format(BMI[0], BMI[1])
    print 'Basal Metabolic Rate: {}'.format(BMR)
    print 'Resting Metabolic Rate: {}'.format(RMR)
    print 'Daily KCAL: {}'.format(daily_kcal)

    ActivityDuration = 57.0
    HR = 130
    ActivityDuration = float(ActivityDuration) / 60.0 # hours
    KcalBurned = calc.HR2Kcal(gender, age, weight, ActivityDuration, HR, VO2max=None)
    print 'Burned KCAL: {}'.format(KcalBurned)
